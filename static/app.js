(function() {
    "use strict";

    var regionSelect = document.getElementById("region");

    if (!regionSelect) {
        return;
    }

    var cityWrapper = document.getElementById("city-wrapper");
    var citySelect = document.getElementById("city_id");

    regionSelect.addEventListener("change", onRegionChange);

    function onRegionChange() {
        regionSelect.disabled = true;
        cityWrapper.style.display = "none";

        var request = new XMLHttpRequest();
        request.addEventListener("load", onGetCities);
        request.open("get", "/get-cities?region_id=" + this.value, true);
        request.send();
    }

    function onGetCities() {
        if (!(this.readyState == 4 && this.status == 200)) {
            alert("На сервере произошла ошибка!");
            return;
        }

        var cities = JSON.parse(this.responseText);

        // Delete all option except the empty one.
        var zeroOption = null;
        while (citySelect.childNodes.length) {
            if (citySelect.firstChild.localName == "option" && citySelect.firstChild.value == "") {
                zeroOption = citySelect.firstChild;
            }
            citySelect.removeChild(citySelect.firstChild);
        }
        if (zeroOption) {
            citySelect.appendChild(zeroOption);
        }

        if (cities.length > 0) {
            // Create options.
            for (var i in cities) {
                var city = cities[i];
                var option = document.createElement("option");
                option.value = city.id;
                var text = document.createTextNode(city.name);
                option.appendChild(text);
                citySelect.appendChild(option);
            }
            cityWrapper.style.display = "block";
        } else {
            cityWrapper.style.display = "none";
        }

        regionSelect.disabled = false;
    }
})();

(function() {
    "use strict";

    var form = document.getElementById("comment-form");

    if (!form) {
        return;
    }

    form.addEventListener("submit", onSubmit);

    function isValidPhone(str) {
        return str.match(/\(\d+\)\s*[\d\-]+$/);
    }

    function isValidEmail(str) {
        return str.match(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/);
    }

    function onSubmit(event) {
        event.preventDefault();

        var errorElements = [];

        var lastName = document.getElementById("last_name");
        lastName.className = "";
        lastName.value = lastName.value.trim();
        if (lastName.value == "") {
            errorElements.push(lastName);
        }

        var firstName = document.getElementById("first_name");
        firstName.className = "";
        firstName.value = firstName.value.trim();
        if (firstName.value == "") {
            errorElements.push(firstName);
        }

        var region = document.getElementById("region");
        region.className = "";
        region.value = region.value.trim();
        if (region.value == "") {
            errorElements.push(region);
        }

        var city = document.getElementById("city_id");
        city.className = "";
        if (city.value == "") {
            errorElements.push(city);
        }

        var phone = document.getElementById("phone");
        phone.className = "";
        phone.value = phone.value.trim();
        if (phone.value != "" && !isValidPhone(phone.value)) {
            errorElements.push(phone);
        }

        var email = document.getElementById("email");
        email.className = "";
        email.value = email.value.trim();
        if (email.value != "" && !isValidEmail(email.value)) {
            errorElements.push(email);
        }

        var comment = document.getElementById("comment");
        comment.className = "";
        comment.value = comment.value.trim();
        if (comment.value == "") {
            errorElements.push(comment);
        }

        for (var i in errorElements) {
            errorElements[i].className = "is-error";
        }

        if (errorElements.length == 0) {
            var request = new XMLHttpRequest();
            request.addEventListener("load", onAddComment);
            request.open("post", "/add-comment", true);
            var params = "";
            params += "last_name=" + encodeURIComponent(lastName.value);
            params += "&first_name=" + encodeURIComponent(firstName.value);
            params += "&patronymic=" + encodeURIComponent(patronymic.value);
            params += "&city_id=" + encodeURIComponent(city.value);
            params += "&phone=" + encodeURIComponent(phone.value);
            params += "&email=" + encodeURIComponent(email.value);
            params += "&comment=" + encodeURIComponent(comment.value);
            request.send(params);
        } else {
            errorElements[0].focus();
        }

        function onAddComment() {
            if (!(this.readyState == 4 && this.status == 200)) {
                alert("На сервере произошла ошибка!");
                return;
            }
            form.reset();
            document.getElementById("city-wrapper").style.display = 'none';
            alert("Комментарий добавлен!");
        }
    }
})();

(function() {
    "use strict";

    var links = document.querySelectorAll("a.delete");

    if (links.length == 0) {
        return;
    }

    for (var i = 0; i < links.length; ++i) {
        var link = links[i];
        link.addEventListener('click', onClick);
    }

    function onClick(event) {
        event.preventDefault();
        var link = this;
        var request = new XMLHttpRequest();
        request.addEventListener("load", function() {
            if (!(this.readyState == 4 && this.status == 200)) {
                alert("На сервере произошла ошибка!");
                return;
            }
            var row = link.parentNode.parentNode;
            row.parentNode.removeChild(row);
        });
        request.open("post", "/delete-comment", true);
        var id = parseInt(this.getAttribute('data-id'));
        var params = "id=" + id;
        request.send(params);
    }
})();
