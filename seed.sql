DROP TABLE IF EXISTS regions;
CREATE TABLE regions(
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL
);
INSERT INTO regions VALUES(1, 'Краснодарский край');
INSERT INTO regions VALUES(2, 'Ростовская область');
INSERT INTO regions VALUES(3, 'Ставропольский край');

DROP TABLE IF EXISTS cities;
CREATE TABLE cities(
    id INTEGER PRIMARY KEY NOT NULL,
    region_id INTEGER NOT NULL,
    name TEXT NOT NULL
);
INSERT INTO cities VALUES(1, 1, 'Краснодар');
INSERT INTO cities VALUES(2, 1, 'Кропоткин');
INSERT INTO cities VALUES(3, 1, 'Славянск');
INSERT INTO cities VALUES(4, 2, 'Ростов');
INSERT INTO cities VALUES(5, 2, 'Шахты');
INSERT INTO cities VALUES(6, 2, 'Батайск');
INSERT INTO cities VALUES(7, 3, 'Ставрополь');
INSERT INTO cities VALUES(8, 3, 'Пятигорск');
INSERT INTO cities VALUES(9, 3, 'Кисловодск');

DROP TABLE IF EXISTS comments;
CREATE TABLE comments(
    id INTEGER PRIMARY KEY NOT NULL,
    last_name TEXT NOT NULL,
    first_name TEXT NOT NULL,
    patronymic TEXT,
    city_id INTEGER NOT NULL,
    phone TEXT,
    email TEXT,
    comment TEXT NOT NULL
);
INSERT INTO comments VALUES(1, 'Иванов', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(2, 'Иванов2', 'Иван', '', 1, '', '', 'Тестовый комментарий');
INSERT INTO comments VALUES(3, 'Иванов3', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(4, 'Иванов4', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(5, 'Иванов5', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(6, 'Иванов6', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(7, 'Иванов7', 'Иван', 'Иванович', 1, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');

INSERT INTO comments VALUES(8, 'Иванов8', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(9, 'Иванов9', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(10, 'Иванов10', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(11, 'Иванов11', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(12, 'Иванов12', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(13, 'Иванов13', 'Иван', 'Иванович', 4, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');

INSERT INTO comments VALUES(14, 'Иванов14', 'Иван', 'Иванович', 7, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');

INSERT INTO comments VALUES(15, 'Иванов15', 'Иван', 'Иванович', 2, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(16, 'Иванов16', 'Иван', '', 2, '', '', 'Тестовый комментарий');
INSERT INTO comments VALUES(17, 'Иванов17', 'Иван', 'Иванович', 2, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(18, 'Иванов18', 'Иван', 'Иванович', 2, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(19, 'Иванов19', 'Иван', 'Иванович', 2, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
INSERT INTO comments VALUES(20, 'Иванов20', 'Иван', 'Иванович', 2, '(123) 456-789', 'ivanov.ivan@gmail.com', 'Тестовый комментарий');
