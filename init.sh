#!/usr/bin/env bash

# Create the db if it doesn't already exist.
touch db.sqlite
# Import the seed file.
cat seed.sql | sqlite3 db.sqlite
