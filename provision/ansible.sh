#!/bin/bash

# Install Ansible and its dependencies if it's not installed already.
if ! command -v ansible >/dev/null; then
  apt-get update

  echo "Installing Ansible dependencies and Git."
  apt-get install -y git python python-dev

  echo "Installing pip via easy_install."
  wget https://raw.githubusercontent.com/ActiveState/ez_setup/v0.9/ez_setup.py
  python ez_setup.py && rm -f ez_setup.py
  easy_install pip
  # Make sure setuptools are installed correctly.
  pip install setuptools --no-use-wheel --upgrade

  echo "Installing required python modules."
  pip install paramiko PyYAML Jinja2 httplib2 six

  echo "Installing Ansible."
  pip install ansible
fi

echo "Running Ansible provisioner."
ansible-playbook -i 'localhost,' "/vagrant/provision/playbook.yml" --connection=local --extra-vars "env=vagrant"
