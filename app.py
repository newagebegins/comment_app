# -*- coding: utf-8 -*-

import sqlite3
import os
import urlparse
import json
import string
import re

class Db:
    def __init__(self):
        script_dir = os.path.realpath(os.path.dirname(__file__))
        db_path = os.path.join(script_dir, 'db.sqlite')
        self.connection = sqlite3.connect(db_path)
        self.connection.row_factory = sqlite3.Row
        self.cursor = self.connection.cursor()

    def close(self):
        self.cursor.close()
        self.connection.close()


layout_html = u"""
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>$page_title</title>
        <link rel="stylesheet" href="/static/app.css">
    </head>
    <body>
        <ul class="menu">$menu_items</ul>
        <h1>$page_title</h1>
        $body
        <script src="/static/app.js"></script>
    </body>
</html>
"""


# Debug print
def dp(item):
    import pprint; pprint.pprint(item)


def get_menu_items_html(request_path):
    output = u''
    menu = [
        ('/comment', u'Добавить комментарий'),
        ('/view', u'Все комментарии'),
        ('/stat', u'Статистика')
    ]
    for path, title in menu:
        cls = ''
        if request_path == path:
            cls = 'is-active'
        output += u'<li class="%s"><a href="%s">%s</a></li>' % (cls, path, title)
    return output


def sanitize_text(s):
    return s.strip().decode('utf-8')


def route_comment(environ, start_response):
    page_template_html = u"""
    <form id="comment-form" method="post">
        <div>
            <label for="last-name" class="is-required">Фамилия</label>
            <input type="text" id="last_name" name="last_name">
        </div>
        <div>
            <label for="first-name" class="is-required">Имя</label>
            <input type="text" id="first_name" name="first_name">
        </div>
        <div>
            <label for="patronymic">Отчество</label>
            <input type="text" id="patronymic" name="patronymic">
        </div>
        <div>
            <label for="region" class="is-required">Регион</label>
            <select id="region" name="region">
                <option value="">- Выберите -</option>
                $region_options
            </select>
        </div>
        <div id="city-wrapper">
            <label for="city" class="is-required">Город</label>
            <select id="city_id" name="city_id">
              <option value="">- Выберите -</option>
            </select>
        </div>
        <div>
            <label for="phone">Контактный телефон</label>
            <input type="text" id="phone" name="phone">
            <div class="help">Формат: (код города) номер</div>
            <div class="help">Пример: (123) 456-789</div>
        </div>
        <div>
            <label for="email">E-mail</label>
            <input type="text" id="email" name="email">
        </div>
        <div>
            <label for="comment" class="is-required">Комментарий</label>
            <textarea id="comment" name="comment" rows="10" cols="70"></textarea>
        </div>
        <div>
            <button type="submit">Отправить</button>
        </div>
    </form>
    """
    db = Db()
    db.cursor.execute('SELECT * FROM regions')
    options = u''
    for region_row in db.cursor:
        options += u'<option value="%d">%s</option>' % (region_row["id"], region_row["name"])
    page_template = string.Template(page_template_html)
    page_body = page_template.substitute(region_options=options)
    layout_template = string.Template(layout_html)
    response_body = layout_template.substitute(page_title=u'Добавить комментарий',
                                               menu_items=get_menu_items_html(environ['app.request_path']),
                                               body=page_body)
    db.close()
    start_response('200 OK', [('Content-type', 'text/html')])
    return [response_body.encode("utf-8")]


def route_view(environ, start_response):
    db = Db()
    query = """
    SELECT comments.id,
           comments.last_name,
           comments.first_name,
           comments.patronymic,
           regions.name AS region,
           cities.name AS city,
           comments.phone,
           comments.email,
           comments.comment
    FROM comments
    LEFT JOIN cities ON comments.city_id = cities.id
    LEFT JOIN regions ON cities.region_id = regions.id
    """
    db.cursor.execute(query)
    page_body = u"""
    <table><tbody>
        <tr>
            <th>ID</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Регион</th>
            <th>Город</th>
            <th>Телефон</th>
            <th>Email</th>
            <th>Комментарий</th>
            <th>Операции</th>
        </tr>
    """
    for row in db.cursor:
        page_body += u'<tr>'
        page_body += u'<td>%s</td>' % row['id']
        page_body += u'<td>%s</td>' % row['last_name']
        page_body += u'<td>%s</td>' % row['first_name']
        page_body += u'<td>%s</td>' % row['patronymic']
        page_body += u'<td>%s</td>' % row['region']
        page_body += u'<td>%s</td>' % row['city']
        page_body += u'<td>%s</td>' % row['phone']
        page_body += u'<td>%s</td>' % row['email']
        page_body += u'<td>%s</td>' % row['comment']
        page_body += u'<td><a href="/delete-comment" data-id="%d" class="delete">Удалить</a></td>' % row['id']
        page_body += u'</tr>'
    page_body += u'</tbody></table>'
    layout_template = string.Template(layout_html)
    response_body = layout_template.substitute(page_title=u'Все комментарии',
                                               menu_items=get_menu_items_html(environ['app.request_path']),
                                               body=page_body)
    db.close()
    start_response('200 OK', [('Content-type', 'text/html')])
    return [response_body.encode("utf-8")]


def route_stat(environ, start_response):
    db = Db()
    layout_template = string.Template(layout_html)
    filter_comment_count = 5
    query = """
    SELECT regions.id,
           regions.name,
           COUNT(comments.id) AS comments_count
    FROM regions
    INNER JOIN cities ON cities.region_id = regions.id
    INNER JOIN comments ON comments.city_id = cities.id
    GROUP BY regions.id
    HAVING comments_count > ?
    """
    db.cursor.execute(query, (filter_comment_count,))
    page_body = u"""
    <p>Регионы с количеством комментариев больше %d:</p>
    <table><tbody>
        <tr>
            <th>Регион</th>
            <th>Кол-во комментариев</th>
        </tr>
    """ % (filter_comment_count,)
    for row in db.cursor:
        page_body += u'<tr>'
        page_body += u'<td><a href="/stat/%d">%s</a></td>' % (row['id'], row['name'])
        page_body += u'<td>%s</td>' % row['comments_count']
        page_body += u'</tr>'
    page_body += u'</tbody></table>'
    response_body = layout_template.substitute(page_title=u'Статистика',
                                               menu_items=get_menu_items_html(environ['app.request_path']),
                                               body=page_body)
    db.close()
    start_response('200 OK', [('Content-type', 'text/html')])
    return [response_body.encode("utf-8")]


def route_stat_region(environ, start_response):
    db = Db()
    region_id = environ['app.url_args'][0]
    db.cursor.execute('SELECT * FROM regions WHERE id = ?', region_id)
    region = db.cursor.fetchone()

    if not region:
        db.close()
        start_response('403 Forbidden', [])
        return ['']

    query = """
    SELECT cities.name,
           COUNT(comments.id) AS comments_count
    FROM cities
    JOIN comments ON comments.city_id = cities.id
    WHERE cities.region_id = ?
    GROUP BY cities.name
    """
    db.cursor.execute(query, region_id)
    page_body = u"""
    <table><tbody>
        <tr>
            <th>Город</th>
            <th>Кол-во комментариев</th>
        </tr>
    """
    for row in db.cursor:
        page_body += u'<tr>'
        page_body += u'<td>%s</td>' % row['name']
        page_body += u'<td>%s</td>' % row['comments_count']
        page_body += u'</tr>'
    page_body += u'</tbody></table>'
    page_title = u'Статистика > %s' % region['name']
    layout_template = string.Template(layout_html)
    response_body = layout_template.substitute(page_title=page_title,
                                               menu_items=get_menu_items_html(environ['app.request_path']),
                                               body=page_body)
    db.close()
    start_response('200 OK', [('Content-type', 'text/html')])
    return [response_body.encode("utf-8")]


def route_get_cities(environ, start_response):
    db = Db()
    query_params = urlparse.parse_qs(environ['QUERY_STRING'])
    db.cursor.execute('SELECT * FROM cities WHERE region_id = ?', query_params['region_id'][0])
    cities = [{ 'id': row['id'], 'name': row['name'] } for row in db.cursor]
    response_body = json.dumps(cities)
    db.close()
    start_response('200 OK', [('Content-type', 'application/json')])
    return [response_body.encode("utf-8")]


def route_add_comment(environ, start_response):
    db = Db()
    request_body = environ['wsgi.input'].readline()
    post_params = urlparse.parse_qs(request_body)

    last_name = sanitize_text(post_params.get('last_name', [''])[0])
    first_name = sanitize_text(post_params.get('first_name', [''])[0])
    patronymic = sanitize_text(post_params.get('patronymic', [''])[0])
    city_id = post_params.get('city_id', [''])[0]
    phone = sanitize_text(post_params.get('phone', [''])[0])
    email = sanitize_text(post_params.get('email', [''])[0])
    comment = sanitize_text(post_params.get('comment', [''])[0])

    no_errors = True

    if last_name == "":
        no_errors = False

    if first_name == "":
        no_errors = False

    db.cursor.execute('SELECT * FROM cities WHERE id = ?', city_id)
    if db.cursor.rowcount == 0:
        no_errors = False

    if phone != "" and not re.match(r'\(\d+\)\s*[\d\-]+$', phone):
        no_errors = False

    if email != "" and not re.match(r"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", email):
        no_errors = False

    if comment == "":
        no_errors = False

    if no_errors:
        values = (None, last_name, first_name, patronymic, city_id, phone, email, comment)
        db.cursor.execute('INSERT INTO comments VALUES(?,?,?,?,?,?,?,?)', values)
        db.connection.commit()
        start_response('200 OK', [])
    else:
        start_response('403 Forbidden', [])
    db.close()
    return ['']


def route_delete_comment(environ, start_response):
    db = Db()
    if environ['REQUEST_METHOD'] == 'POST':
        request_body = environ['wsgi.input'].readline()
        post_params = urlparse.parse_qs(request_body)
        id = post_params.get('id', [''])[0]
        if id:
            start_response('200 OK', [('Content-type', 'text/plain')])
            db.cursor.execute('DELETE FROM comments WHERE id = ?', (id,))
            db.connection.commit()
        else:
            start_response('403 Forbidden', [])
    else:
        start_response('403 Forbidden', [])
    db.close()
    return ['']


def route_catch_all(environ, start_response):
    start_response('301 Moved Permanently', [('Location', '/comment')])
    return ['']


def application(environ, start_response):
    routes = [
        (r"^/comment$", route_comment),
        (r"^/view$", route_view),
        (r"^/stat$", route_stat),
        (r"^/stat/(\d+)$", route_stat_region),
        (r"^/get-cities$", route_get_cities),
        (r"^/add-comment$", route_add_comment),
        (r"^/delete-comment$", route_delete_comment)
    ]

    request_path = environ['PATH_INFO'].rstrip('/')
    for regex, callback in routes:
        match = re.search(regex, request_path)
        if match is not None:
            environ['app.url_args'] = match.groups()
            environ['app.request_path'] = request_path
            return callback(environ, start_response)

    return route_catch_all(environ, start_response)
